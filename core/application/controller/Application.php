<?php
namespace txs\application\controller;
session_start();
require_once 'core/login/controller/UserLoginController.php';
require_once 'core/post/controller/ShowPosts.php';
require_once 'core/post/controller/AddPost.php';
require_once 'core/post/controller/EditPost.php';
require_once 'core/post/controller/DeletePost.php';
require_once 'core/comments/controller/EditComment.php';
require_once 'core/comments/controller/AddComment.php';
require_once 'core/comments/controller/ShowComment.php';
require_once 'core/comments/controller/DeleteComment.php';
require_once 'core/dashboard/controller/DashBoard.php';
require_once 'core/usersettings/controller/UserSettings.php';
require_once 'core/common/view/Navigation.php';
/**
* Application controller
*/
class Application {
    /**
     * @var 
     */
    private $route;

    /**
     * @var \txs\post\controller\
     */
    private $showPosts;      
    /**
     * @var \txs\post\controller\
     */
    private $addPost;        
    /**
     * @var \txs\post\controller\
     */
    private $deletePost;     
    /**
     * @var \txs\post\controller\
     */
    private $editPost;       
    /**
     * @var \txs\comments\controller\
     */
    private $addComment;     
    /**
     * @var \txs\comments\controller\
     */
    private $showComment;    
    /**
     * @var \txs\comments\controller\
     */
    private $deleteComment;  
    /**
     * @var \txs\comments\controller\
     */
    private $editComment;    
    /**
     * @var \txs\dashboard\controller\
     */
    private $dashboard;      
    /**
     * @var \txs\usersettings\controller\
     */
    private $usersettings;   
    /**
     * @var \txs\login\controller\
     */
    private $loginController;

    public function __construct($route) {
        $this->route           = $route;
        $this->showPosts       = new \txs\post\controller\ShowPosts();
        $this->addPost         = new \txs\post\controller\AddPost();
        $this->deletePost      = new \txs\post\controller\DeletePost();
        $this->editPost        = new \txs\post\controller\EditPost();
        $this->addComment      = new \txs\comments\controller\AddComment();
        $this->showComment     = new \txs\comments\controller\ShowComment();
        $this->deleteComment   = new \txs\comments\controller\DeleteComment();
        $this->editComment     = new \txs\comments\controller\EditComment();
        $this->dashboard       = new \txs\dashboard\controller\DashBoard();
        $this->usersettings    = new \txs\usersettings\controller\UserSettings();
        $this->loginController = new \txs\login\controller\UserLoginController();
    }

    public function init() {
        /* ==========================================================================
           Dashboard \ Main Routes
           ========================================================================== */
        
        $this->route->all("/dash.*", function() {
            if (!$this->loginController->userIsLoggedIn()) {
                \txs\common\view\Navigation::toLogin();
            }
        });

        $this->route->get("/dash/", function() {
            try {
                return $this->dashboard->doDashboardFront() . $this->showPosts->showPostsTitles() . $this->dashboard->getDashStats();
            } catch (\Exception $e) {
                \txs\common\view\Navigation::toError();
            }
            
        });
        /* ==========================================================================
            Dashboard \ Posts   
        ========================================================================== */
        $this->route->get("/dash/:id/:slug", function($id) {
            try {
                return $this->dashboard->doDashboardFront() . $this->editPost->getForm($id);
            } catch (\Exception $e) {
                \txs\common\view\Navigation::toError();
            }

        });  

        $this->route->put("/dash/:id/:slug", function($id) {
            try {
                $this->editPost->editPost($id);
               \txs\common\view\Navigation::toDash();            
            } catch (\Exception $e) {
                return $this->dashboard->doDashboardFront() . $this->editPost->getForm($id);
            }
        });  
        $this->route->delete("/dash/:id/:slug", function($id) {

            try {
                $this->deletePost->deletePost($id);
                \txs\common\view\Navigation::toDash(); 
            } catch (\Exception $e) {
                \txs\common\view\Navigation::toError();
            }

        }); 
               
        $this->route->get("/dash/new", function() {
            try {
                return $this->dashboard->doDashboardFront() . $this->addPost->getForm();
            } catch (\Exception $e) {
                \txs\common\view\Navigation::toError();
            }
        });   
        $this->route->post("/dash/new", function() {
            try {
                $this->addPost->addPost();
                \txs\common\view\Navigation::toDash();    
            } catch (\Exception $e) {
                return $this->dashboard->doDashboardFront() . $this->addPost->getForm();                
            }
            
        });

        /* ==========================================================================
           Dashboard \ Settings
           ========================================================================== */
        $this->route->post("/dash/settings", function() {
            try {
                $this->usersettings->saveSettings();
                \txs\common\view\Navigation::toDash();
            } catch (\Exception $e) {
                return $this->dashboard->doDashboardFront() . $this->usersettings->getSettingsForm();
            }

        });
        $this->route->get("/dash/settings", function() {
            try {
                return $this->dashboard->doDashboardFront() . $this->usersettings->getSettingsForm();
            } catch (\Exception $e) {
                \txs\common\view\Navigation::toError();
            }
        });

        /* ==========================================================================
           Posts Routes
           ========================================================================== */
        $this->route->get("/:id/:slug", function($id, $slug) {
            try {
                return $this->showPosts->showPost($id, $slug) . $this->showComment->getComments($id);
            } catch (\Exception $e) {
                return $this->showPosts->notFound();
            }

        });      


        $this->route->get("/", function() {
            try {
                return $this->showPosts->showPosts();
            } catch (\Exception $e) {
                \txs\common\view\Navigation::toError();
            }

        });

        /* ==========================================================================
           Comments
           ========================================================================== */


        $this->route->get("/dash/p/:id/comment/:id", function($postId, $commentId) {
            try {
                return $this->editComment->getEditForm($postId, $commentId);
            } catch (\Exception $e) {
                \txs\common\view\Navigation::toError();
            }
        });    

        $this->route->post("/:id/:slug", function($postId, $slug) {
            try {
                $this->addComment->addComment($postId);
                \txs\common\view\Navigation::toPost($postId, $slug);
            } catch (\Exception $e) {
                \txs\common\view\Navigation::toPost($postId, $slug);
            }

        });    
        $this->route->put("/dash/p/:id/comment/:id", function($postId, $commentId) {
            try {
                $this->editComment->editComment($postId, $commentId);    
                \txs\common\view\Navigation::toPost($postId, $commentId);
            } catch (\Exception $e) {
               return $this->editComment->getEditForm($postId, $commentId); 
            }
            
            

        }); 
        $this->route->delete("/dash/p/:id/comment/:id", function($postId, $commentId) {
            try {
                $this->deleteComment->deleteComment($commentId);
                \txs\common\view\Navigation::toPost($postId, $commentId);
            } catch (\Exception $e) {
                \txs\common\view\Navigation::toError();
            }

        });        
        
        /* ==========================================================================
           Login Routes
           ========================================================================== */
        $this->route->get("/login", function() {

            
            return $this->loginController->doLoginFront();

        });
        $this->route->post("/login", function() {

            
            return $this->loginController->doLogin();

        });
        $this->route->get("/logout", function() {
    
            $this->loginController->doLogout();
            \txs\common\view\Navigation::toFront();
        });        


        /* ==========================================================================
           Error
           ========================================================================== */

        $this->route->error(function() {
            return \txs\common\view\Navigation::notFound();
        });
        
        
        /* ==========================================================================
           Start router
           ========================================================================== */
     
        return $this->route->run();
    }
}