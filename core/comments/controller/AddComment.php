<?php
namespace txs\comments\controller;
require_once 'core/comments/controller/Comment.php';
require_once 'core/comments/view/AddComment.php';

/**
* Comments Controller
*/
class AddComment extends Comment {


    /* ==========================================================================
     *   $this->userLoginModel  = new \txs\login\model\UserLoginModel();
     *   $this->commentsManager = new \txs\comments\model\CommentManager();      
    ========================================================================== */

    /**
     * @var \txs\comments\view\
     */
    private $addComment;

    public function __construct() {
        parent::__construct();
        $this->addComment = new \txs\comments\view\AddComment($this->commentsManager);
    }


    public function addComment($id) {
        $comment = $this->addComment->getNewComment($id);
        $this->commentsManager->addComment($comment);
    }


}