<?php
namespace txs\comments\controller;
require_once 'core/comments/view/BaseCommentsView.php';
require_once 'core/comments/model/CommentsManager.php';
require_once 'core/login/model/UserLoginModel.php';
/**
* SuperClass Comment controller
*/
class Comment {
    /**
     * @var \txs\login\model\
     */
    protected $userLoginModel;
    /**
     * @var \txs\comments\model\
     */
    protected $commentsManager;

    
    public function __construct() {
        $this->userLoginModel  = new \txs\login\model\UserLoginModel();
        $this->commentsManager = new \txs\comments\model\CommentManager();
        
    }
}