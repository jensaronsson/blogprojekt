<?php
namespace txs\comments\controller;
require_once 'core/comments/view/DeleteComment.php';
/**
* DeleteComment
*/
class DeleteComment extends Comment {
    

    /* ==========================================================================
     *   $this->userLoginModel  = new \txs\login\model\UserLoginModel();
     *   $this->commentsManager = new \txs\comments\model\CommentManager();      
    ========================================================================== */
    /**
     * @var \txs\comments\view\
     */
    private $deleteComment;

    public function __construct() {
        parent::__construct();
        $this->deleteComment = new \txs\comments\view\DeleteComment($this->commentsManager);
    }


    // smells a bit funny
    public function deleteComment($commentId) {
        $comment = $this->deleteComment->getCommentDel($commentId);
        $this->commentsManager->deleteComment($comment);
    }
    

}