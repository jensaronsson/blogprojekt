<?php
namespace txs\comments\controller;
require_once 'core/comments/controller/Comment.php';
require_once 'core/comments/view/EditComment.php';
/**
* EditComment
*/
class EditComment extends Comment {
    

    /* ==========================================================================
     *   $this->userLoginModel  = new \txs\login\model\UserLoginModel();
     *   $this->commentsManager = new \txs\comments\model\CommentManager();      
    ========================================================================== */
    /**
     * @var \txs\comments\view\
     */
    private $editComment;


    public function __construct() {
        parent::__construct();
        $this->editComment = new \txs\comments\view\EditComment($this->commentsManager);
    }

    public function getEditForm($postId, $commentId) {
        $this->commentsManager->getComment($postId, $commentId);
        return $this->editComment->getEditForm();
    }

    public function editComment($postId, $commentId) {
        $comment = $this->editComment->getNewComment($postId, $commentId);
        $this->commentsManager->editComment($comment);
    }

}