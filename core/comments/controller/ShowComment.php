<?php
namespace txs\comments\controller;
require_once 'core/comments/controller/Comment.php';
require_once 'core/comments/view/ShowComment.php';

/**
* ShowComment
*/
class ShowComment extends Comment {
    /**
     * @var \txs\comments\view\
     */
    private $showComment;

    public function __construct() {
        parent::__construct();
        $this->showComment = new \txs\comments\view\ShowComment($this->commentsManager); 
    }


    public function getComments($postId) {
        $isLoggedIn = $this->userLoginModel->sessionExists();
        $this->commentsManager->getComments($postId);
        return $this->showComment->getComments($isLoggedIn);
    }
}