<?php
namespace txs\comments\model;
/**
* Comment
*/
class Comment {
    
    /**
     * @var String
     */
   private  $commentId;
   /**
    * @var String
    */
   private  $comment;
   /**
    * @var String
    */
   private  $name;
   /**
    * @var String
    */
   private  $published;
   /**
    * @var String
    */
   private  $postId;





    public function __construct($comment, $name, $postId, $published = null, $commentId = null) {
        if (!is_null($comment) && !is_string($comment)) {
            throw new \Exception("Invalid comment");
        }
        if (!is_null($name) && !is_string($name)) {
            throw new \Exception("Invalid name");
        }
        if (!is_null($postId) &&!is_string($postId)) {
            throw new \Exception("Invalid postId");
        }                        
        if (!is_null($published) && !is_string($published) ) {
            throw new \Exception("Invalid published format");   
        }
        if (!is_null($commentId) && !is_string($commentId) ) {
            throw new \Exception("Invalid commentId format");   
        }                  
        $this->commentId = $commentId;
        $this->comment   = $comment;
        $this->name      = $name;
        $this->published = $published;
        $this->postId    = $postId;
    }
    /**
     * @return String HTML
     */
    public function getCommentId() {
        return $this->commentId;
    }
    /**
     * @return String HTML
     */
    public function getComment() {
        return $this->comment;
    }
    /**
     * @return String HTML
     */
    public function getName() {
        return $this->name;
    }
    /**
     * @return String HTML
     */
    public function getPublished() {
        $date = strtotime($this->published);
        return date('Y-m-d', $date);
    }
    /**
     * @return String HTML
     */
    public function getPostId() {
        return $this->postId;
    }
}