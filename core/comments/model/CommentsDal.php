<?php
namespace txs\comments\model;
require_once 'core/common/model/DB.php';
require_once 'core/comments/model/Comment.php';
/**
* CommentsDAL
*/
class CommentsDAL {
    /**
     * @var \txs\common\model\
     */
    private static $db;

    public function __construct() {
        self::$db = new \txs\common\model\DB(); 
    }

    public function addComment(\txs\comments\model\Comment $comment) {

        $stmt = self::$db->prepare("INSERT INTO comment (`comment`, `name`,`postId`) VALUES( :comment, :name, :postId)");
        $stmt->execute(array(':comment' => $comment->getComment(),
                            ':name' => $comment->getName(),
                            ':postId' => $comment->getPostId()
                            ));

    }

    public function editComment(\txs\comments\model\Comment $comment) {
        $stmt = self::$db->prepare("UPDATE comment SET comment = :comment, name = :name WHERE postId = :postId AND commentId = :commentId");
        $stmt->execute(array(':comment' => $comment->getComment(),
                            ':name' => $comment->getName(),
                            ':postId' => $comment->getPostId(),
                            ':commentId' => $comment->getCommentId()
                            ));
    }


    public function deleteComment(\txs\comments\model\Comment $comment) {
        $stmt = self::$db->prepare("DELETE FROM comment WHERE commentId = :commentId");
        $stmt->execute(array(':commentId' => $comment->getCommentId()));
    }

    public function getComment($postId, $commentId) {
        $stmt = self::$db->prepare("SELECT * FROM comment WHERE postId = :postId AND commentId = :commentId");
        $stmt->execute(array(':postId' => $postId, ':commentId' => $commentId));      
        $comments = array();
            while ($comment = $stmt->fetch(\PDO::FETCH_ASSOC)) {
                return new \txs\comments\model\Comment($comment["comment"],
                                                      $comment["name"],
                                                      $comment['postId'],
                                                      $comment["published"],
                                                      $comment['commentId']);
            }
            if ($comments == null) {
                throw new \Exception("No comment found by provided id");
            } else {
                return $comments;
            }
    }


    public function getCommentsById($postId) {
        $stmt = self::$db->prepare("SELECT * FROM comment WHERE postId = :postId ORDER BY published DESC");
        $stmt->execute(array(':postId' => $postId));
        $comments = array();

        while ($comment = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $comments[] = new \txs\comments\model\Comment(
                                                          $comment["comment"],
                                                          $comment["name"],
                                                          $comment['postId'],
                                                          $comment["published"],
                                                          $comment['commentId']);
        }
        return $comments;
        
    }
}