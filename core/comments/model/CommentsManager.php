<?php
namespace txs\comments\model;
require_once 'core/comments/model/CommentsDal.php';
/**
* CommentManager
*/
class CommentManager {
        
    /**
    * @var \txs\comments\model\
    */
    private $commentsDal;   

    public function __construct() {
        $this->commentsDal = new \txs\comments\model\CommentsDal();
    }

    public function getComments($postId) {
        $this->comments = $this->commentsDal->getCommentsById($postId);       
    }

    public function getComment($postId, $commentId) {
        $this->comments = $this->commentsDal->getComment($postId, $commentId);
    }

    public function editComment(\txs\comments\model\Comment $comment) {
        $this->commentsDal->editComment($comment);
    }

    public function addComment(\txs\comments\model\Comment $comment) {
        $this->commentsDal->addComment($comment);
    }

    public function deleteComment(\txs\comments\model\Comment $comment) {
        $this->commentsDal->deleteComment($comment);
    }
}