<?php
namespace txs\comments\view;
/**
* Comments View
*/
class AddComment extends BaseCommentsView {
    
    /**
     * @param  String $postId 
     * @return \txs\comments\model\
     */
    public function getNewComment($postId = null) {
        $comment   = $this->getComment();
        $name      = $this->getName();
        $this->checkErrors();
        
        return new \txs\comments\model\Comment($comment, $name, $postId);
    }




}