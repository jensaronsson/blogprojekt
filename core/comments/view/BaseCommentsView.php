<?php
namespace txs\comments\view;
require_once 'core/comments/model/CommentsManager.php';
/**
* BaseCommentsView  
*/
class BaseCommentsView {
    
    /**
     * @var \txs\comments\model\CommentManager
     */
    protected $comments;
    /**
     * @var array
     * Contains errors to be thrown in Exceptions.
     */
    protected $errors = array();
    /**
     * @var string
     */
    private static $sessionError   = "BaseCommentsView::Error";
    private static $exceptionError = "Something wrong happend";
    private static $commentName    = "comment-name";
    private static $commentText    = "comment-text";

    public function __construct(\txs\comments\model\CommentManager $commentManager) {
        $this->comments = $commentManager;
    }
    /**
     * Collects errors and print them out at the same
     * @return String
     */
    protected function getError() {
        if (isset($_SESSION[self::$sessionError])) {
            $error = "<div class='error'><ul>" . $_SESSION[self::$sessionError] . "</ul></div>";
            unset($_SESSION[self::$sessionError]);
            return $error;
        }
    }

    private function addError($error) {
        isset($_SESSION[self::$sessionError]) ?
        $_SESSION[self::$sessionError] .= $error 
        : $_SESSION[self::$sessionError] = $error; 
    }

    protected function checkErrors() {
        if (!empty($this->errors)) {
            $errors = $this->errors;
            $this->errors = "";
            throw new \Exception(print_r($errors, TRUE));
        }
    }
        
    protected function getComment() {
        if (isset($_POST[self::$commentText])) {
            $text   = \txs\common\Filter::sanitize($_POST[self::$commentText]);
            if (strlen($text) <= 0) {
                $errorMsg = "<li>Comment cant be empty</li>";
                $this->addError($errorMsg);
                $this->errors[] = $errorMsg;
            }            
            return $text;
        }
        throw new \Exception(self::$exceptionError);
    }
    protected function getName() {
        if (isset($_POST[self::$commentName])) {
            $name     = \txs\common\Filter::sanitize($_POST[self::$commentName]);
            $badChars = \txs\common\Filter::checkBadChars($_POST[self::$commentName]);
            if (strlen($name) <= 0) {
                $errorMsg = "<li>Name cant be empty</li>";
                $this->addError($errorMsg);
                $this->errors[] = $errorMsg;
            } elseif ($badChars) {
                $errorMsg = "<li>Name must only contain a-z åäö letters and -! characters</li>";
                $this->addError($errorMsg);
                $this->errors[] = $errorMsg;
            }            
            return $name;
        }
        throw new \Exception(self::$exceptionError);
    }






}