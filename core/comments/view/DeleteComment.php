<?php
namespace txs\comments\view;
/**
* DeleteComment
*/
class DeleteComment extends BaseCommentsView {
    
    /**
     * @param  string $commentId
     * @return Strint HTML
     */
    public function getCommentDel($commentId) {
        
        return new \txs\comments\model\Comment(null, null, null, null, $commentId);   
    }
}