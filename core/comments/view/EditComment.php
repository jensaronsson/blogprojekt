<?php
namespace txs\comments\view;
/**
* EditComments
*/
class EditComment extends BaseCommentsView {
    


    public function getEditForm() {
        $comment = $this->comments->comments;    
        $html  = $this->getError();
        $html .= "<section class='comments edit'>
                 <h3 class='edit-comment-title'>Edit Comment</h3>
                 <form class='comments-form' method='POST' action=''>
                    <input type='hidden' name='_method' value='PUT'>
                    <label for='comment-text'>Comment:</label>
                    <textarea id='comment-text' name='comment-text'>" . $comment->getComment() . "</textarea>
                    <input type='text' id='name' name='comment-name' value='" . $comment->getName() . "' placeholder='Name'>
                    <label for='name'>Name:</label>
                    <input type='submit' name='submit-comment' value='Update'>
                </form>";
        $html .= "</section>";               
        return $html;
    }


    /**
     * @param  String $postId
     * @param  String $commentId 
     * @return String HTML
     */
    public function getNewComment($postId, $commentId) {
        $comment = $this->getComment();
        $name    = $this->getName();
        $this->checkErrors();

        return new \txs\comments\model\Comment($comment, $name, $postId, null, $commentId);
    }
    
}