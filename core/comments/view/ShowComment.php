<?php
namespace txs\comments\view;
/**
* ShowComment
*/
class ShowComment extends BaseCommentsView {
    

    public function getComments($isLoggedIn) {
        $html = "<section class='comments'>";
        $html .= $this->getCommentsForm();
        $html .= $this->getCommentsHtml($isLoggedIn);
        $html .= "</div>";
        return $html;
    }

    private function getCommentsHtml($isLoggedIn) {
        $comments = $this->comments->comments;
        $html = "";
                foreach ($comments as $comment) {
           
           $html .= "<article class='comment'>
                    <section class='meta-date'><p>" . $comment->getName() . " • ". $comment->getPublished()  . "</p></section>
                   <p>" . nl2br($comment->getComment()) . "</p>";
                   if ($isLoggedIn) {
                       $html .= "<section class='user-action'>
                                    <form action='/dash/p/" . $comment->getPostId() . "/comment/" . $comment->getCommentId() . "' method='POST'>
                                        <input type='hidden' name='_method' value='DELETE'>
                                        <input type='submit' name='delete' value='Delete'>
                                        <a href='/dash/p/" . $comment->getPostId() . "/comment/" . $comment->getCommentId() . "'>Edit</a>
                                    </form>
                                </section>";
                        $html .= "</article>";
                   }
                        $html .= "</article>";
                }
        return $html;
    }

    protected function getCommentsForm() {
        $html = $this->getError();
        $html .= "<form class='comments-form' method='POST' action=''>
                    <textarea id='comment-text' name='comment-text' placeholder='Got something to say?'></textarea>
                    <input type='text' id='name' name='comment-name' placeholder='Name'>
                    <input type='submit' name='submit-comment' value='Comment'>
                </form>";
        return $html;
    }

}