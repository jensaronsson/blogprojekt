<?php
namespace txs\common;
/**
* Filter
*/
class Filter {
    

    /**
     * @param  string $string
     * @return String
     */
    public static function sanitize($string) {

        $string = ltrim($string);
        $string = rtrim($string);
        $string = strip_tags($string);
        return $string;
    }


    /**
     * @param  String $arg
     * @return boolean
     */
    public static function checkBadChars($title) {
        preg_match("/^[\sa-zåäöÅÄÖ0-9-!]+$/i", $title, $matches);
        if (!$matches) {
            return true;
        } else {
            return false;
        }
    }
}