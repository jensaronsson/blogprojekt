<?php
namespace txs\common\model;
/**
* DAlbase
*/
class DB extends \PDO {
    
    private static $host     = "localhost";
    private static $dbname   = "blog";
    private static $username = "root";
    private static $password = "root"; 
    private static $charset  = "UTF8";
    private static $port     = "8889";

    public function __construct() {
        try {
            parent::__construct("mysql:host=".self::$host.";dbname=".self::$dbname.";
                port=".self::$port.";charset=".self::$charset.";", self::$username, self::$password);
            $this->setAttribute(parent::ATTR_ERRMODE, parent::ERRMODE_EXCEPTION);
            
        } catch (Exception $e) {
            echo " error : " . $e->getMessage();
        }        
    }

}