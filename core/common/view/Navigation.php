<?php
namespace txs\common\view;
/**
* Navigation    
*/
class Navigation  {
    
    /**
     * @return String Html
     */
    public static function notFound() {
        $html = "<h1> Something wrong happend</h1>
                <a href='/'>Return to startpage</a>";
        return $html;
    }

    public static function toError() {
        header("Location: /notfound");
    }

    public static function toDash() {
        header("Location: /dash/");
    }

    public static function toPost($id, $slug) {
        header("Location: /$id/$slug");
    }

    public static function toFront() {
        header("Location: /");
    }
    public static function toLogin() {
        header("Location: /login");
    }

    public static function reLoad() {
        header("Location: /login");
    }    
}