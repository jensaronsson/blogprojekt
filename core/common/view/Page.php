<?php 
namespace txs\common\view;
/**
* Common Page
*/
class Page {



    /**
     * @param  String $title 
     * @param  String $body
     * @return String HTML
     */
    public function getPage($title, $body) {
        $html = "<!doctype html>
                <html lang='sv'>
                <head>
                    <meta charset='UTF-8'>
                    <title>$title</title>
                    <link rel='stylesheet' href='/public/stylesheets/css/style.css'>
                </head>
                <body>

                    <main>
                        $body
                    <footer class='master'></footer>
                    </main>
                    
                </body>
                </html>";

        return $html;        
    }
}