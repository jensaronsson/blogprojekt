<?php
namespace txs\dashboard\controller;
require_once 'core/dashboard/view/DashBoard.php';
require_once 'core/dashboard/model/DashStatsManager.php';
require_once 'core/login/model/UserLoginModel.php';

/**
* DashboardController
*/
class Dashboard {
    
    /**
     * @var \txs\dashboard\view
     */
    private $dashView;
    /**
     * @var \txs\login\model\
     */
    private $userLoginModel;
    /**
     * @var \txs\dashboard\model\
     */
    private $dashStatsManager;
    
    public function __construct() {
        $this->userLoginModel   = new \txs\login\model\UserLoginModel();
        $this->dashStatsManager = new \txs\dashboard\model\DashStatsManager();
        $this->dashView         = new \txs\dashboard\view\DashBoard($this->dashStatsManager);

    }

    public function getDashStats() {
        return $this->dashView->getDashStats();
    }


    public function doDashboardFront() {
        $loggedInUser = $this->userLoginModel->getLoggedInUserName();
        return $this->dashView->getDashboard($loggedInUser);
    }
}