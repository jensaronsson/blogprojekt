<?php
namespace txs\dashboard\model;
/**
* CommentStats
*/
class CommentStats {
    /** 
     * @var String
     */
    private $title;
    /** 
     * @var String
     */    
    private $amount;
    /** 
     * @var String
     */    
    private $publised;

    public function __construct($title, $amount = null, $published = null) {
        if (!is_string($title)) {
            throw new \Exception("Invalid title format");
        }
        if (!is_null($amount) && !is_string($amount) ) {
            throw new \Exception("Invalid amount format");   
        }                
        if (!is_null($published) && !is_string($published) ) {
            throw new \Exception("Invalid published format");   
        }          
        $this->title = $title;
        $this->amount = $amount;
        $this->publised = $published;
    }

    /**
     * @return String
     */
    public function getTitle() {
        return $this->title;
    }
    /**
     * @return String
     */
    public function getAmount() {
        return $this->amount;
    }
}