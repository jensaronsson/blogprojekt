<?php
namespace txs\dashboard\model;
require_once 'core/common/model/DB.php';
require_once 'core/dashboard/model/CommentStats.php';

/**
* DashStatsDal
*/
class DashStatsDal {
    /**
     * @var \txs\common\model
     */
    private static $db;
    
    public function __construct() {
        self::$db = new \txs\common\model\DB();        
    }


    public function getCommentStats() {
        $stmt = self::$db->prepare("SELECT p.title, count(c.comment) as amountComments From post as p
                                    JOIN comment as c ON c.postId = p.postId 
                                    GROUP BY c.postId
                                    ORDER BY c.comment DESC LIMIT 3");
        $stmt->execute();
        $comments = array();
        while ($res = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $comments[] = new \txs\dashboard\model\CommentStats($res['title'], $res['amountComments']);     
        } 
        return $comments;
        
    }

    public function getLatestComments() {
        $stmt = self::$db->prepare("SELECT c.comment From comment as c
                                    ORDER BY c.published DESC LIMIT 6");
        $stmt->execute();
        
        $comments = array();
        while ($res = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $comments[] = new \txs\dashboard\model\CommentStats($res['comment']);     
        } 
        return $comments;
    }


    public function getOverallStats() {
        $stmt = self::$db->prepare("SELECT
                                    (SELECT count(postId) FROM post) as totalpost,
                                    (SELECT count(comment)FROM comment) as totalcomments");

        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $res;
    }

}