<?php
namespace txs\dashboard\model;
require_once 'core/dashboard/model/DashStatsDal.php';

/**
* DashStatsManager
*/
class DashStatsManager {
    
    public function __construct() {
        $this->dashStatsDal = new \txs\dashboard\model\DashStatsDal();
    }

    public function getCommentStats() {
        return $this->dashStatsDal->getCommentStats();
    }

    public function getOverallStats() {
        return $this->dashStatsDal->getOverallStats();
    }

    public function getLatestComments() {
        return $this->dashStatsDal->getLatestComments();
    }
}