<?php
namespace txs\dashboard\view;

/**
* DashBoard
*/
class DashBoard {

    /**
     * @var \txs\dashboard\model\DashStatsManager
     */
    private $getDashStatsManager;
    
    public function __construct(\txs\dashboard\model\DashStatsManager $dashStatsManager) {
        $this->getDashStatsManager = $dashStatsManager;
    }
    /**
     * @param String $username
     * @return String html
     */
    private function getNavigationHtml($username) {
        $html = "<header>
                    <nav id='dashboard'>
                        <div id='dashboard-nav'>
                            <ul>
                                <li><a href='/'>Front</a></li>
                                <li><a href='/dash/'>Posts</a></li>
                                <li><a href='/dash/new'>New Post</a></li>
                            </ul>
                        </div>
                        <div id='dashboard-user'>
                            <ul>
                                <li><a href='/dash/'>$username</a>
                                    <ul>
                                        <li><a href='/dash/settings'>Settings</a></li>
                                        <li><a href='/logout'>Logout</a></li>
                                    </ul>   
                                </li> 
                            </ul>
                        </div>             
                    </nav>
                </header>";
        return $html;
    }
    /**
     * @return String html
     */
    private function getPopular() {
        $popular = $this->getDashStatsManager->getCommentStats();
        $html =  "<article class='dash-popular-post'>
                        <h3>Top 3 Posts:</h3>";
                        foreach ($popular as $comment) {
                            $html .= "<span><section class='dash-popular-title'>" . $comment->getTitle() . "</section>";
                            $html .= "<section class='dash-popular-comment'>" . $comment->getAmount() .  " comments </section></span>";
                        }
        $html .= "</article>";
        return $html;
    }
    /**
     * @return String html
     */
    private function getLatestComments() {
        $latestComments = $this->getDashStatsManager->getLatestComments();
        $html = "<article class='dash-latest-comments'>
                        <h3>Latest comments</h3>";
                        foreach ($latestComments as $comment) {
                            
                            $html .= "<p>" . $comment->getTitle() . "</p>";
                        }
               $html .= "</article>";
        return $html;
    }
    /**
     * @return String html
     */
    private function getOverallStats() {
        $stats = $this->getDashStatsManager->getOverallStats();
        $html = "<article class='dash-stats-total'>
                    <h3>Blog statistics:</h3>
                    <section class='total-posts'><p>" . $stats["totalpost"] . "</p></section>
                    <section class='total-comments'><p>" . $stats["totalcomments"] . "</p></section>
                </article>";
        return $html;
    }
    /**
     * @return String html
     */
    public function getDashStats() {
        $html = "<div class='dash-stats'>";
                $html .= $this->getOverallStats();
                $html .= $this->getPopular();
                $html .= $this->getLatestComments();   
                $html .= "</div>";
        return $html;
    }
    /**
     * @return String html
     */
    public function getDashBoard($username) {
        return $this->getNavigationHtml($username);
    }           
}