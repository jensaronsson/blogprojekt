<?php
namespace txs\login\controller;

require_once 'core/login/view/LoginView.php';
require_once 'core/login/model/UserLoginModel.php';
require_once 'core/login/UserLoginException.php';

class UserLoginController {

    /**
     * @var \Model\UserloginModel
     */
    private $userLoginModel;
    /**
     * @var \View\LoginView
     */
    private $userLoginView;

    public function __construct() {
        $this->userLoginModel = new \txs\login\model\UserLoginModel();
        $this->userLoginView  = new \txs\login\view\LoginView($this->userLoginModel);
    }

    
    public function userIsLoggedIn() {
        return $this->userLoginModel->sessionExists(); 
    }

    public function doLogout() {
        if($this->userLoginModel->sessionExists()) {
           return $this->logoutHandler();
        }
    }
    /**
     * @return String Html
     */
    public function doLoginFront() {
        
        try {    
            if($this->userLoginModel->sessionExists()) {
                return \txs\common\view\Navigation::toDash();
            } 
        } catch (\Exception $e) {
            return $this->userLoginView->getForm();
        }
        return $this->userLoginView->getForm();
    }

    public function doLogin() {
     
        if($this->userLoginView->isSubmitted()) {
           return $this->loginHandler();
        }
        return $this->userLoginView->getForm();        
    }
    


    /**
     * @return String HTML
     */
    private function logoutHandler() {
        $this->userLoginModel->unsetSession();
        $this->userLoginView->setMessage(\txs\login\view\LoginView::LOGOUT_SUCCESS);
        return $this->userLoginView->getForm();    
    }

    /**
     * @return String HTML
     */
    private function loginHandler() {
      try {
            $usernameInput = $this->userLoginView->getUsername();
            $passwordInput = $this->userLoginView->getPassWord();

            $user = $this->userLoginModel->checkUsername($usernameInput);                

            $this->userLoginModel->authUser($user->getPassword(), $passwordInput);
            $this->userLoginModel->setSession($user);
           
            return \txs\common\view\Navigation::toDash();
        } catch (\common\UserLoginException $e) {
            $this->userLoginView->setMessage(\txs\login\view\LoginView::WRONG_CRED);
            return $this->userLoginView->getForm();
        }catch (\Exception $e) {
            return $this->userLoginView->getForm();
        } 
    }
}