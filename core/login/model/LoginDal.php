<?php
namespace txs\login\model;
require_once 'core/login/model/User.php';
require_once 'core/common/model/DB.php';
/**
* LoginDal
*/
class LoginDal {

    /**
     * @var PDO
     */
    private static $db;

    public function __construct() {
       self::$db = new \txs\common\model\DB(); 
    }

    public function fetchUserByUserName($username) {
        $stmt = self::$db->prepare("SELECT * FROM user WHERE username = :username");
        $stmt->execute(array(':username' => $username));
        $user = $stmt->fetch(\PDO::FETCH_ASSOC);
        return new \txs\login\model\User($user['userId'], $user['username'], $user['password']); 
    }

    public function saveCookie($user) {
        $stmt = self::$db->prepare("UPDATE user SET token = :token WHERE userId = :userId");
        $stmt->execute(array(':userId' => $user->getUserId(), ':token' => $user->getToken()));
    }
    public function fetchCookie($cookiename) {
        $stmt = self::$db->prepare("SELECT * FROM user WHERE token LIKE '$cookiename:%'");
        $stmt->execute();
        $user = $stmt->fetch(\PDO::FETCH_ASSOC);

        return  new \txs\login\model\User($user['userId'], $user['username'], $user['password'], $user['token']); 
        
    }
}