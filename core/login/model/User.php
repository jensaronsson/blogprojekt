<?php
namespace txs\login\model;
/**
* User
*/
class User {
    /**
     * @var String
     */
    private $userId;
    /**
     * @var String
     */    
    private $username;
    /**
     * @var String
     */    
    private $password;


    
    public function __construct($userId, $username, $password) {
        if (!is_string($userId)) {
            throw new \Exception("Invalid userId format");
        }
        if (!is_string($username)) {
            throw new \Exception("Invalid username format");
        }
        if (!is_string($password)) {
            throw new \Exception("Invalid password format");
        }        

        $this->userId   = $userId;
        $this->username = $username;
        $this->password = $password;
    }
    /**
     * @return String
     */
    public function getUserName() {
        return $this->username;
    }
    /**
     * @return String
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * @return String
     */
    public function getUserId() {
        return $this->userId;
    }


}