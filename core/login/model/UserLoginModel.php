<?php
namespace txs\login\model;
require_once 'core/login/model/LoginDal.php';
require_once 'core/login/model/User.php';

class UserLoginModel {

    /**
     * @var Strings
     */
    private static $usersession   = "LoginModel::user";
    private static $sessionsecure = "SECURE";
    private static $useragent     = "HTTP_USER_AGENT";
    private static $ip            = "REMOTE_ADDR";
    private static $sessionLogin  = "Login";
    private static $sessionUser   = "LoginModel::user::username";
    private static $sessionUserId = "LoginModel::user::userId";
    private $user;



    public function __construct() {
        $this->loginDal = new \txs\login\model\LoginDal();
    }

    /**
     * @return boolean 
     */
    public function sessionExists() {
        if (!empty($_SESSION[self::$usersession])) {
            if($_SESSION[self::$usersession][self::$useragent]
               != $_SERVER[self::$useragent] ||
               $_SESSION[self::$usersession][self::$ip]
               != $_SERVER[self::$ip]) {
                throw new \Exception("Session theft! Catch hen! I think hen went that way! ->");
            }
        }
        return isset($_SESSION[self::$usersession][self::$sessionLogin]);
    }
    
    /**
     * Sets session, saves ip adress and useragent
     */
    public function setSession($user) {
        $_SESSION[self::$usersession] = array();
        $_SESSION[self::$usersession][self::$useragent]      = $_SERVER[self::$useragent];
        $_SESSION[self::$usersession][self::$ip]             = $_SERVER[self::$ip];
        $_SESSION[self::$usersession][self::$sessionLogin]   = true;
        $_SESSION[self::$usersession][self::$sessionUser]    = $user->getUserName();
        $_SESSION[self::$usersession][self::$sessionUserId]  = $user->getUserId();
    }

    /**
     * @return boolean
     */
    public function unsetSession() {
        if(isset($_SESSION[self::$usersession][self::$sessionLogin])) {
            session_unset($_SESSION[self::$usersession][self::$sessionLogin]);
            return true;  
        }
    }

    public function getLoggedInUserName() {
        return $_SESSION[self::$usersession][self::$sessionUser];
    }

    public function getLoggedInUserId() {
        return $_SESSION[self::$usersession][self::$sessionUserId];
    }
  

    /**
     * @param  String
     * @return User Object
     * @throws UserLoginException
     */
    public function checkUsername($username) {

        try {
            $this->user = $this->loginDal->fetchUserByUserName($username);
            return $this->user;
        } catch (\Exception $e) {
            throw new \common\UserLoginException("No match for that username");
                    
        }
        
    }

    /**
     * @param  String $usrname
     * @param  String $pswd
     * @return boolean
     * @throws Exception
     */
    public function authUser($password1, $password2) {

            if($password1 === $password2 && $password1 === $password2) {
                return true; 
            } else {
                if($password1 !== $password2 || $password1 !== $password2) {
                    throw new \common\UserLoginException("Wrong username/password");
                }
            }    
        
        
    }






}