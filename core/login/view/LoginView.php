<?php
namespace txs\login\view;
class LoginView {
    /**
     * @var String
     */
    private $username;
    /**
     * @var String
     */
    private $password;
    /**
     * @var String
     */
    private $message;
    /**
     * @var \model\userLoginModel
     */
    private $userLoginModel;

    /**
     * @var String 
     */

    private static $userNameInput   = "email";
    private static $passwordInput   = "password";
    private static $submitted       = "submitted";
    const LOGOUT_SUCCESS            = "Du har nu loggats ut";
    const WRONG_CRED                = "Wrong username or password";


    /**
     * @param type \model\userLoginModel $userLoginModel 
     */
    public function __construct(\txs\login\model\userLoginModel $userLoginModel) {
        $this->password = "";
        $this->username = "";
        $this->message  = "";
        $this->userLoginModel = $userLoginModel;
    }





    /**
     * Returns a username with no whitespace and no html characters
     * @param String $input 
     * @return String
     */
    private function sanitize($input) {
        return html_entity_decode(trim($input));
    }
    /**
     * @return String username
     * @throws Exception if usersname is empty
     */
    public function getUserName() {
        $username = $this->sanitize($_POST[self::$userNameInput]);
        if(strlen($username) >= 1) {
            $this->username = $username;
            return $this->username;
        } else {
            $this->setMessage("Username is missing");
            throw new \Exception("Username is missing");
        }
    }
    /**
     * Returns a password with no whitespaces and no htmlcharacters
     * @return String password
     * @throws Exception if password is empty.
     */
    public function getPassWord() {
        $password = $this->sanitize($_POST[self::$passwordInput]);
        if(strlen($password) >= 1 ) {
            $this->password = $password;
            return $this->password;
        } else {
            $this->setMessage("Password is missing");
            throw new \Exception("Password is missing");
        }
    }

    public function setMessage($msg) {
        $this->message = $msg;
    }

    /**
     * @return Boolean 
     */
    public function isSubmitted() {
        return isset($_POST[self::$submitted]);
    }
 
    /**
     * @return HTML
     */
    public function getForm() {
        return "
            <div class='login-form'>
            <h2>Sign in</h2>
				<form action='login' id='login' method='POST'>
                <h3>" . $this->message . "</h3>
					<input type='text' value='" . $this->username . "'
                            name='" . self::$userNameInput . "' id='username' placeholder='Username'>
					<input type='password' name='" 
                    . self::$passwordInput . "' id='password' placeholder='Password'>

					<input type='submit' 
                    name='" . self::$submitted . "' value='Log in'>
				</form>
			</div>
        ";
    }
}