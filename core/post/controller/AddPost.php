<?php 
namespace txs\post\controller;
require_once 'core/post/controller/Post.php';
require_once 'core/post/view/AddPost.php';

/**
* AddPost controller for handling adding posts
*/
class AddPost extends Post {

    /***************************************************************************
                            Inhereited from Post 
    ----------------------------------------------------------------------------
    *   $this->postDAL        = new \txs\post\model\PostDAL();
    *   $this->postManager    = new \txs\post\model\PostManager($this->postDAL);
    *   $this->userLoginModel = new \txs\login\model\UserLoginModel();
    *   $this->settingsManager = new \txs\usersettings\model\SettingsManager();
    ***************************************************************************/
    
    /**
     * @var \txs\post\view
     */
    private $addPost;

    public function __construct() {
        parent::__construct();
        $this->addPost = new \txs\post\view\AddPost();
    }

    public function addPost() {
        $newPost = $this->addPost->addPost($this->userLoginModel);
        $this->postManager->addPost($newPost);
    }

    public function getForm() {
        return $this->addPost->getPostForm();
    }
}