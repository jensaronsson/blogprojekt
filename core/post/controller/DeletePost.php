<?php
namespace txs\post\controller;
require_once 'core/post/controller/Post.php';
/**
* DeletePost
*/
class DeletePost extends Post {
    
    /***************************************************************************
                            Inhereited from Post 
    ----------------------------------------------------------------------------
    *   $this->postDAL        = new \txs\post\model\PostDAL();
    *   $this->postManager    = new \txs\post\model\PostManager($this->postDAL);
    *   $this->userLoginModel = new \txs\login\model\UserLoginModel();
    *   $this->settingsManager = new \txs\usersettings\model\SettingsManager();
    ***************************************************************************/

    public function __construct() {
        parent::__construct();
    }

    public function deletePost($id) {
        $post = $this->postManager->getSinglePost($id);
        $this->postManager->deletePost($post);
        
    }
}