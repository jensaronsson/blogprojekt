<?php 
namespace txs\post\controller;
require_once 'core/post/view/EditPost.php';
require_once 'core/post/controller/Post.php';

/**
* EditPost controller for handling adding posts
*/
class EditPost extends Post {

    /***************************************************************************
                            Inhereited from Post 
    ----------------------------------------------------------------------------
    *   $this->postDAL        = new \txs\post\model\PostDAL();
    *   $this->postManager    = new \txs\post\model\PostManager($this->postDAL);
    *   $this->userLoginModel = new \txs\login\model\UserLoginModel();
    *   $this->settingsManager = new \txs\usersettings\model\SettingsManager();
    ***************************************************************************/

    /**
     * @var \txs\post\view
     */
    private $editPost;
    
    public function __construct() {
        parent::__construct();
        $this->editPost = new \txs\post\view\editPost();
    }

    public function editPost($id) {
        $editPost = $this->editPost->editPost($id, $this->userLoginModel);
        $this->postManager->editPost($editPost);
    }


    public function getForm($id) {
        $post = $this->postManager->getSinglePost($id);
        return $this->editPost->getPostForm($post);
    }
}