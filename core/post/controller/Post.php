<?php
namespace txs\post\controller;
require_once 'core/login/model/UserLoginModel.php';
require_once 'core/usersettings/model/SettingsManager.php';
require_once 'core/post/model/PostDal.php';
require_once 'core/post/model/PostManager.php';
require_once 'core/post/model/Post.php';
require_once 'core/post/view/BasePostView.php';
require_once 'core/common/Filter.php';


/**
* Post
*/
class Post {
    
    /**
     * @var \txs\post\model
     */
    protected $postDAL;
    /**
     * @var \txs\login\model\
     */
    protected $userLoginModel;
    /**
     * @var \txs\post\model\
     */
    protected $postManager;
    /**
     * @var \txs\usersettings\model\
     */
    protected $settingsManager;

    
    public function __construct() {
        $this->postDAL         = new \txs\post\model\PostDAL();
        $this->postManager     = new \txs\post\model\PostManager($this->postDAL);
        $this->userLoginModel  = new \txs\login\model\UserLoginModel();
        $this->settingsManager = new \txs\usersettings\model\SettingsManager();
        
    }

}