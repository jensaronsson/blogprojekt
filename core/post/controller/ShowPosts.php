<?php 
namespace txs\post\controller;
require_once 'core/post/controller/Post.php';
require_once 'core/post/view/PostList.php';
require_once 'core/post/view/SinglePost.php';
/**
* ShowPosts controller 
*/
class ShowPosts extends Post {
    /***************************************************************************
                            Inhereited from Post 
    ----------------------------------------------------------------------------
    *   $this->postDAL        = new \txs\post\model\PostDAL();
    *   $this->postManager    = new \txs\post\model\PostManager($this->postDAL);
    *   $this->userLoginModel = new \txs\login\model\UserLoginModel();
    *   $this->settingsManager = new \txs\usersettings\model\SettingsManager();
    ***************************************************************************/

    /**
     * @var \txs\post\view\
     */
    private $singlePost;
    /**
     * @var \txs\post\view\
     */
    private $postList;    

    public function __construct() {
        parent::__construct();
        $this->postList    = new \txs\post\view\PostList($this->postManager, $this->settingsManager);
        $this->singlePost  = new \txs\post\view\SinglePost($this->postManager, $this->userLoginModel, $this->settingsManager);
    }

    public function showPosts() {
        return $this->postList->getPostList();

    }
    public function notFound() {
        return $this->singlePost->getPostNotFound();
    }

    public function showPost($id, $slug) {
        return $this->singlePost->getSinglePost($id, $slug);
    }

    public function showPostsTitles() {
        return $this->postList->getPostTitles();
    }
}