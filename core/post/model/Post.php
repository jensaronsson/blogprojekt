<?php 
namespace txs\post\model;
/**
* Post model. Taking care of create posts objects
*/
class Post {
    
    /**
     * @var String
     */
    private $text;
    /**
     * @var String
     */
    private $title;
    /**
     * @var String
     */
    private $authorId;
    /**
     * @var String Date
     */
    private $publishedDate;
    /**
     * @var String
     */
    private $slug;
    /**
     * @var String
     */
    private $postId;    
    /**
     * @var String
     */
    private $fullname;  

    public function __construct($text, $title, $authorId, $fullname = null,
                                $username = null, $postId = null, $publishedDate = null) {
        if (!is_string($text) || $text == "") {
            throw new \Exception("Invalid text");
        }
        if (!is_string($title) || $title == "") {
            throw new \Exception("Invalid title");
        }
        if (!is_string($authorId)) {
            throw new \Exception("Invalid authorId");
        }      

        $this->postId        = $postId;
        $this->text          = $text;
        $this->fullname      = $fullname;
        $this->title         = $title;
        $this->username      = $username;
        $this->authorId      = $authorId;
        $this->publishedDate = $publishedDate;
        $this->makeSlug($this->title);
    }

    /**
     * @param  String $title 
     * @return String [e.g :  Varför då => varfor-da]
     */
    private function makeSlug($title) {
        $title = strtolower($title);
        $patterns = array("/([\s+])/", "/([\.\,\?+])/", "/å|ä/", "/ö/");
        $replace = array("-", "", "a", "o");
        $title = preg_replace($patterns, $replace, $title);
        $title = trim($title, "-");
        $this->slug = $title;
    }



    /**
     * @return String 20 chars
     */
    public function getExcerpt() {
         $text = substr($this->text,0, 200);
         $text .= "...";
         return $text;
    }

    /**
     * @return String
     */
    public function getSlug() {
        return $this->slug;
    }
    /**
     * @return String
     */    
    public function getPostId() {
        return $this->postId;
    }
    /**
     * @return String
     */    
    public function getText() {
        return $this->text;
    }
    /**
     * @return String
     */    
    public function getTitle() {
        return $this->title;        
    }  
    /**
     * @return String
     */      
    public function getAuthorId() {
        return $this->authorId;
    }
    /**
     * @return String
     */    
    public function getUserName() {
        return $this->username;
    }    
    /**
     * @return String
     */    
    public function getFullName() {
        return $this->fullname;
    }      
    /**
     * @return String
     */    
    public function getPublishedDate() {
        $date = strtotime($this->publishedDate);
        return date('Y-m-d', $date);
    }

}