<?php 
namespace txs\post\model;
require_once 'core/common/model/DB.php';
require_once 'core/post/model/Post.php';
/**
* PostDal:
* Data access layer for post
*/
class PostDal {
    
    /**
     * @var \txs\common\model\
     */
    private static $db;

    public function __construct() {
       self::$db = new \txs\common\model\DB(); 
    }

    public function getAllPosts() {
        $stmt = self::$db->prepare("SELECT p.text, p.title, u.username, u.fullname, p.authorId, p.postId, p.published FROM post as p
                                    JOIN user as u on u.userId = p.authorId
                                    ORDER BY published DESC");
        $stmt->execute();
        $posts = array();
        while ($post = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $posts[] = new \txs\post\model\Post($post["text"], $post["title"], $post["authorId"], $post['fullname'], $post['username'],$post["postId"], $post["published"]);
        }
        return $posts;
    }


    public function addPost(\txs\post\model\Post $post) {
        $stmt = self::$db->prepare("INSERT INTO post (text, title, authorId) VALUES (:text, :title, :authorid)");
        $stmt->execute(array(':title' => $post->getTitle(), ':text' => $post->getText(), ':authorid' => $post->getAuthorId()));

        $id = self::$db->lastInsertId();
        

    }

    public function editPost(\txs\post\model\Post $post) {
       
        $updated_on = date('Y-m-d H:i:s',time());
        $stmt = self::$db->prepare("UPDATE post SET text = :text, title = :title, authorId = :authorid, updated_on = :updated WHERE postId = :postId");
        $stmt->execute(array(':title'   => $post->getTitle(),
                            ':text'     => $post->getText(),
                            ':authorid' => $post->getAuthorId(),
                            ':postId'   => $post->getPostId(),
                            ':updated'  => $updated_on));
    }

    public function deletePost(\txs\post\model\Post $post) {
        $stmt = self::$db->prepare("DELETE FROM post WHERE postId = :postId");
        $stmt->execute(array(':postId' => $post->getPostId()));
    }
}