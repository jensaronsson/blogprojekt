<?php 
namespace txs\post\model;
/**
* PostManager
*/
class PostManager {
    
    /**
     * @var \txs\post\model;
     */
    private $postDal;

    /**
     * @var Array
     */
    private $posts;

    public function __construct(\txs\post\model\PostDal $postDal) {
        $this->postDal = $postDal;
        $this->posts = $this->postDal->getAllPosts();
    }

    public function getAllPosts() {
        return $this->posts;
    }


    public function getSinglePost($id) {
        foreach ($this->posts as $post) {
            if ($post->getPostId() == $id) {
                return $post;
            } 
        }
        throw new \Exception("No match in database for that id");
    }

    public function addPost(\txs\post\model\Post $post) {
        $this->postDal->addPost($post);
    }

    public function editPost(\txs\post\model\Post $post) {
        $this->postDal->editPost($post);
    }
    public function deletePost(\txs\post\model\Post $post) {
        $this->postDal->deletePost($post);
    }


}