<?php
namespace txs\post\view;
/**
* AddPost View
*/
class AddPost extends BasePostView {
    /**
     * @var string
     */
    private static $title = "";
    /**
     * @var string
     */    
    private static $text  = "";

    public function getPostForm() {

        $html = $this->getError();
        $html .= "<div class='dash-form'>";
        $html .= "<form method='post' action='./new'>
                    <input id='title' name='title' type='title' value='".self::$title."' placeholder='Blog title'>
                    <textarea id='text' name='text' placeholder='Whats on your mind?'>" . self::$text . "</textarea>
                    <input type='submit' value='Publish' id='post-publish'>
                </form>";
        $html .= "</div>";                
        return $html;
    }    

    public function addPost($userLoginModel) {
        $title = $this->getTitle();
        $text  = $this->getText();
        self::$title = $title;
        self::$text  = $text;
        $this->checkErrors();
        $authorId = $userLoginModel->getLoggedInUserId();
        return new \txs\post\model\Post($text, $title, $authorId);
    }
}