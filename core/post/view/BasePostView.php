<?php
namespace txs\post\view;
/**
* BasePostView
*/
class BasePostView  {
    /**
     * @var array
     * Contains errors to be thrown in Exceptions.
     */
    protected $errors = array();
    /**
     * @var string
     */
    private static $sessionError   = "BasePostView::Error";
    private static $exceptionError = "Something wrong happend";
    private static $postTitle      = "title";
    private static $postText       = "text";

    /**
     * Collects errors and print them out at the same
     * @return String
     */
    protected function getError() {
        if (isset($_SESSION[self::$sessionError])) {
            $error = "<div class='error'><ul>" . $_SESSION[self::$sessionError] . "</ul></div>";
            unset($_SESSION[self::$sessionError]);
            return $error;
        }
    }
    
    protected function checkErrors() {
        if (!empty($this->errors)) {
            throw new \Exception(print_r($this->errors, TRUE));
        }
    }
    /**
     * @param add error to session. 
     * Create if there isnt already error in session
     */
    private function addError($error) {
        isset($_SESSION[self::$sessionError]) ?
        $_SESSION[self::$sessionError] .= $error 
        : $_SESSION[self::$sessionError] = $error; 
    }
    /**
     * @return String
     * @throws Exception If blogtitle doesnt contain letters
     */
    protected function getTitle() {
        if (isset($_POST[self::$postTitle])) {
            $title   = \txs\common\Filter::sanitize($_POST[self::$postTitle]);
            $badChars = \txs\common\Filter::checkBadChars($title);
            if (strlen($title) <= 0) {
                $errorMsg = "<li>Blogtitle cant be empty</li>";
                $this->addError($errorMsg);
                $this->errors[] = $errorMsg;
            } elseif ($badChars) {
                $errorMsg = "<li><span class='italic'>Only a-z åäö 0-9 -!</span> characters are allowed</li>";
                $this->addError($errorMsg);
                $this->errors[] = $errorMsg;
            }
            return $title;
        }
        throw new \Exception(self::$exceptionError);
        
    }




    /**
     * @return String
     * @throws Exception If blogtitle doesnt contain letters
     */
    protected function getText() {
        if (isset($_POST[self::$postText])) {
            $text = \txs\common\Filter::sanitize($_POST[self::$postText]);
            if (strlen($text) <= 0) {
                $errorMsg = "<li>Blogtext can't be empty</li>";
                $this->addError($errorMsg);
                $this->errors[] = $errorMsg;
            }
            return $text;
        }
        throw new \Exception(self::$exceptionError);
    }

    /**
     * @param  String $title
     * @return String HTML
     */
    protected function getPostHeader($title) {
       $html = "<header id='main-header'>
                    <div class='inner-header'>
                        <div class='v-middle'>
                            <div class='site-bio'>
                                 <h1 class='site-title'><a href='/'>$title</a></h1>
                            </div>
                        </div>
                    </div>
                </header>";
        return $html;
    }
    /**
     * @param  String $title
     * @return String HTML
     */
    protected function getPostFooter($bio, $fullName) {
       $html = "<footer class='master-singlepost'>
                    <div class='inner'>
                        <span class='about-author'>
                            <h3>$fullName</h3>
                            <p>$bio</p>
                        </span>                            
                    </div>
                </footer>";
        return $html;
    }


}