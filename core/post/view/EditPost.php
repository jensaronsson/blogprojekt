<?php
namespace txs\post\view;
/**
* EditPost
*/
class EditPost extends BasePostView {
    
    public function getPostForm($post) {

        $html = $this->getError();
        $html .= "<div class='dash-form'>";
        $html .= "<form method='POST' action=''>
                    <input type='hidden' name='_method' value='PUT'>
                        <input id='title' name='title' value='".$post->getTitle()."' type='title' maxlength='50'>
                        <textarea id='text' name='text'>".$post->getText()."</textarea>
                    <input type='submit' id='post-update' value='Update'>
                </form>
                <form method='POST' action=''>
                    <input type='hidden' name='_method' value='DELETE'>
                    <input type='submit' id='post-delete' value='Delete'>
                </form>";
        $html .= "</div>";       
        return $html;
    }

    public function editPost($id, $userLoginModel) {
        $title = $this->getTitle();
        $text  = $this->getText();
        if (!empty($this->errors)) {
            throw new \Exception(print_r($this->errors, TRUE));
        }
        
        $authorId = $userLoginModel->getLoggedInUserId();
        return new \txs\post\model\Post($text,$title, $authorId, null,null, $id);
    }
}