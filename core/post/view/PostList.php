<?php 
namespace txs\post\view;
/**
* PostList View handling rendering posts
*/
class PostList extends BasePostView {
    

    /**
     * @var \txs\post\model\PostManager
     */
    private $postManager;
    /**
     * @var \txs\usersettings\model\SettingsManager
     */    
    private $settingsManager;

    public function __construct(\txs\post\model\PostManager $postManager,
                                \txs\usersettings\model\SettingsManager $settingsManager) {
        $this->postManager = $postManager;  
        $this->settingsManager = $settingsManager;
    }


    /**
     * @return String HTML
     */
    public function getPostList() {
        $posts    = $this->postManager->getAllPosts();
        $settings = $this->settingsManager->loadSettings();
        $html = $this->getPostHeader($settings['title']);
            foreach ($posts as $post) {
                $html .= "<article class='post'>
                            <header>
                            <section class='meta-date'>"
                            . $post->getPublishedDate() . 
                            " • " . $post->getUserName() .
                            "</section>
                               <h2 class='post-title'><a href='". $post->getPostId() . "/" . $post->getSlug() . "'>"
                                . $post->getTitle() .
                               "</a></h2>
                            </header>
                            <section><p>"
                                . $post->getExcerpt() . 
                            "</p></section>";

                $html .= "</article>";   
            }
        return $html;
    } 

    /**
     * This schould be refactored to dash domain.
     * @return String html
     */
    public function getPostTitles() {
        $posts = $this->postManager->getAllPosts();
        
        $html = "<div class='dash-postlist'>";
        foreach ($posts as $post) {
                $html .= "<article class='dash-post'>
                            <header>
                            <a href='/dash/" . $post->getPostId() . "/" . $post->getSlug() . "'>
                               <h2 class='dash-post-title'>"
                                . $post->getTitle() .
                               "</h2></a>
                               </header> 
                            <section class='meta-author'> <p>Author: ". $post->getUserName() . "</p></section>
                            <section class='meta-date'> 
                                <time datetime='" . $post->getPublishedDate() . "'>posted " . $post->getPublishedDate() . "</time>
                            </section>";


                $html .= "</article>";   
            }
        $html .= "</div>";
        return $html;
    }
}