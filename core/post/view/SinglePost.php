<?php 
namespace txs\post\view;
/**
* SinglePage Html
*/
class SinglePost extends BasePostView {
    /**
     * @var \txs\post\model\PostManager
     */
    private $postManager;
    /**
     * @var \txs\login\model\UserLoginModel
     */
    private $userLoginModel;
    /**
     * @var \txs\usersettings\model\SettingsManager
     */
    private $settingsManager;

    
    public function __construct(\txs\post\model\PostManager $postManager,
                                \txs\login\model\UserLoginModel $userLoginModel,
                                \txs\usersettings\model\SettingsManager $settingsManager) {
        $this->postManager = $postManager;
        $this->userLoginModel = $userLoginModel;
        $this->settingsManager = $settingsManager;
    }
    
    /**
     * @return String Html
     */
    public function getPostNotFound() {
        return "<h1>Post not found</h1>
                <br>
                <a href='/'> Return to startpage</a>";
    }

    /**
     * @param String $id 
     * @param String $slug 
     * @return String HTML
     */
    public function getSinglePost($id, $slug) {
        $post = $this->postManager->getSinglePost($id);
        $settings = $this->settingsManager->loadSettings($post->getAuthorId());
        // Checks if the slug is correct
        // If wrong, it redirects to correct.
        if ($post->getSlug() != $slug) {
            \txs\common\view\Navigation::toPost($id, $post->getSlug());
    }

    $html = $this->getPostHeader($post->getTitle());
    $html .= "<article class='post'>
                <header>
                   <section class='meta-date'>"
                   . $post->getPublishedDate() .  
                   "</section>
                   <h2 class='single-post-title'>"
                    . $post->getTitle() .
                   "</h2>
                </header>
                <section><p>"
                    . nl2br($post->getText()) . 
                "</p></section>
                </article>";
    $html .= $this->getPostFooter($settings['bio'], $post->getFullName());              
    return $html;
    }  
}