<?php
namespace txs\usersettings\controller;
require_once 'core/usersettings/view/UserSettings.php';
require_once 'core/usersettings/model/SettingsManager.php';
require_once 'core/login/model/UserLoginModel.php';
/**
* Settings
*/
class UserSettings {
    
    /**
     * @var \txs\login\model\
     */
    private $userLoginModel;
    /**
     * @var \txs\usersettings\model\
     */
    private $settingsManager;
    /**
     * @var \txs\usersettings\view\
     */
    private $settings;

    public function __construct() {
        $this->userLoginModel  = new \txs\login\model\UserLoginModel();
        $this->settingsManager = new \txs\usersettings\model\SettingsManager($this->userLoginModel);
        $this->settings        = new \txs\usersettings\view\UserSettings($this->settingsManager, $this->userLoginModel);
    }

    /**
     * @return String HTML
     */
    public function getSettingsForm() {
        return $this->settings->getForm();
    }

    
    public function saveSettings() {
        $settings = $this->settings->getNewSettings($this->userLoginModel);
        $this->settingsManager->saveSettings($settings);
    }
}