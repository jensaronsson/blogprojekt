<?php
namespace txs\usersettings\model;
require_once 'core/usersettings/model/UserSettingsDal.php';
/**
* SettingsManager
*/
class SettingsManager  {
    
    /**
     * @var array settings
     */
    private $settings;
    /**
     * @var \txs\usersettings\model\
     */
    private $userSettingsDal;
    public function __construct() {
        $this->userSettingsDal = new \txs\usersettings\model\UserSettingsDal();
    }

    public function loadSettings($userLoginModel = null) {
        return $this->userSettingsDal->loadSettings($userLoginModel);
    }


    public function saveSettings($settings) {
        $this->userSettingsDal->save($settings);
    }



}