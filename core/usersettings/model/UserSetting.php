<?php
namespace txs\usersettings\model;

/**
* Settings
*/
class UserSetting {
    
    public function __construct($blogTitle, $bio, $userId) {
        if (!is_string($blogTitle)) {
            throw new \Exception("Invalid Blogtitle");
        }
        if (!is_string($bio)) {
            throw new \Exception("Invalid bio");
        }
        if (!is_string($userId)) {
            throw new \Exception("Invalid userId");
        }
        if (strlen($blogTitle) == 0) {
            $blogTitle = "Just another blog";
        }
        $this->blogTitle = $blogTitle;
        $this->bio       = $bio;
        $this->userId    = $userId;
    }

    /**
     * @return String
     */
    public function getBlogTitle() {
        return $this->blogTitle;
    }
    /**
     * @return String
     */
    public function getBio() {
        return $this->bio;
    }

    /**
     * @return String
     */
    public function getUserId() {
        return $this->userId;
    }

}