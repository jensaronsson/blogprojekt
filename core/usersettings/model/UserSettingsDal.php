<?php
namespace txs\usersettings\model;
/**
* UserSettingsDal
*/
class UserSettingsDal {
    /**
     * @var \txs\common\model\
     */
    private static $db;

    public function __construct() {
        self::$db = new \txs\common\model\DB();
    }


    public function save(\txs\usersettings\model\UserSetting $usersetting) {
        $sql = "UPDATE settings SET title = :title, key_id = :key WHERE settingsId = 1;
                UPDATE user SET bio = :bio WHERE userId = :userId";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(':title' => $usersetting->getBlogTitle(), ':key' => 'title',
                             ':bio' => $usersetting->getBio(), ':userId' => $usersetting->getUserId()));
    }

    /**
     * Loads settings either for all users or specifik user
     * @param  String $userId 
     * @return $settings array
     */
    public function loadSettings($userId = null) {
        $stmt = self::$db->prepare("SELECT s.title, u.bio FROM settings as s
                                    JOIN user as u
                                    WHERE u.userId = :userId");

        if ($userId == null) {
            $stmt->execute(array(':userId' => "1>0"));
        } else {
            $stmt->execute(array(':userId' => $userId));
        }
        $settings = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $settings;
    }
}