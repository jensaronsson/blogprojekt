<?php
namespace txs\usersettings\view;
require_once 'core/usersettings/model/UserSetting.php';
/**
* UserSettings
*/
class UserSettings {
    /**
     * @var array
     * Contains errors to be thrown in Exceptions.
     */
    protected $errors = array();
    /**
     * @var string
     */
    private static $sessionError   = "UserSettingsView::Error";
    private static $exceptionError = "Something wrong happend";
    private static $badCharsMsg    = "<span class='italic'>Only a-z 0-9 -!</span> characters are allowed";
    private static $blogTitle    = "blogtitle";
    private static $authorBio    = "bio";
    
    /**
     * @var \txs\usersettings\model
     */
    private $settingsManager;
    /**
     * @var \txs\login\model\UserLoginModel
     */
    private $userLoginModel;

    public function __construct(\txs\usersettings\model\SettingsManager $settingsManager,
                                \txs\login\model\UserLoginModel $userLoginModel) {
        $this->settingsManager = $settingsManager;
        $this->userLoginModel  = $userLoginModel;
    }
    /**
     * Collects errors and print them out at the same
     * @return String
     */
    protected function getError() {
        if (isset($_SESSION[self::$sessionError])) {
            $error = "<div class='error'><ul>" . $_SESSION[self::$sessionError] . "</ul></div>";
            unset($_SESSION[self::$sessionError]);
            return $error;
        }
    }
    
    /**
     * Prints errors as exceptions if there is any.
     */
    protected function checkErrors() {
        if (!empty($this->errors)) {
            throw new \Exception(print_r($this->errors, TRUE));
        }
    }

    /**
     * Adds an error to session
     * @param String $error
     */
    private function addError($error) {
        isset($_SESSION[self::$sessionError]) ?
        $_SESSION[self::$sessionError] .= $error 
        : $_SESSION[self::$sessionError] = $error; 
    }

    public function getBlogTitle() {
        if (isset($_POST[self::$blogTitle])) {
            $title   = \txs\common\Filter::sanitize($_POST[self::$blogTitle]);
            $badChars = \txs\common\Filter::checkBadChars($title);
            if ($badChars && strlen($title) > 0) {
                $errorMsg = "<li>". self::$badCharsMsg . "</li>";
                $this->addError($errorMsg);
                $this->errors[] = $errorMsg;
            }
            return $title;
        }
        throw new \Exception(self::$exceptionError);
    }

    public function getBio() {
        if (isset($_POST[self::$authorBio])) {
            $bio   = \txs\common\Filter::sanitize($_POST[self::$authorBio]);
            return $bio;
        }
        throw new \Exception(self::$exceptionError);
    }

    public function getForm() {
        $settings = $this->settingsManager->loadSettings($this->userLoginModel->getLoggedInUserId());
        $html = $this->getError();
        $html .= "<section class='dash-settings'>
                    <h2>Blogsettings</h2>
                    <form action='' method='POST'>
                        <label for='blogtitle'>Blogtitle <br>(defaults to \"Just another blog\"</label>
                        <input type='text' name='blogtitle' value='" . $settings['title'] . "' placeholder='A blog has been born. Please name it!''>
                        <label for='bio'>Who are you? max 200chars</label>
                        <textarea maxlength='200' name='bio' placeholder='Who are you?'>" . $settings['bio'] . "</textarea>
                        <input type='submit' value='Save'>
                    </form>
                </section>";
        return $html;
    }

    /**
     * @return \txs\usersettings\model
     */
    public function getNewSettings() {
        $blogtitle = $this->getBlogTitle();
        $bio       = $this->getBio();
        $userId    = $this->userLoginModel->getLoggedInUserId();
        $this->checkErrors();
        return new \txs\usersettings\model\UserSetting($blogtitle, $bio, $userId);
    }
}