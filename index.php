<?php 
require_once 'core/application/controller/Application.php';
require_once 'core/common/view/Page.php';
require_once 'lib/Router/RouterHandler.php';




$route = new RouterHandler();   
$app   = new \txs\application\controller\Application($route);
$html = $app->init();

$page = new \txs\common\view\Page();

echo $page->getPage("Blog", $html);
