<?php 
/**
* Route class 
*/
class Route {
    
    /**
     * @var String
     */
    private $route;
    /**
     * @var Anonymous function
     */
    private $callback;    


    public function __construct($route, $callback) {
        if (!is_string($route)) {
            throw new \Exception("route is not in a valid format");
        }
        if (!is_callable($callback)) {
            throw new \Exception("Callback is not an valid function");
            
        }
        $this->route      = $route;
        $this->callback   = $callback;
        $this->escapeRoute();
        $this->paramToRegEx();
    }

    public function paramToRegEx() {
        $pattern = array('/(:id)/', '/(:name)/', '/(:slug)/');
        $replace = array('(\d+)', '([a-z+])', '(.+)$');
        $this->route = preg_replace($pattern, $replace , $this->route);
    }

    public function escapeRoute() {
        $this->route = preg_replace("(/)", "\/", $this->route);
    }

    public function getRoute() {
        return $this->route;
    }
    public function getCallback() {
        return $this->callback;
    }
}