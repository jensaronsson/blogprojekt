<?php 
require_once 'Route.php';
require_once 'exceptions/NotFoundException.php';

/**
* Router class for handling request and serve
* right methods and classes
*/
class RouterHandler
{   
    /**
     * @var string
     */
    private $uri;
    /**
     * @var string
     */
    private $routes;
    /**
     * @var string
     */
    private $request;
    /**
     * @var string
     */
    private $rootDir;
    /**
     * @var string
     */
    private $_request;

    /**
     * @var string
     */
    private static $all = "all";
    /**
     * @var string
     */    
    private static $error = "error";

    public function __construct()
    {
        
        $this->rootDir = $_SERVER['DOCUMENT_ROOT'];
        $this->uri     = isset($_REQUEST['uri']) ? "/".$_REQUEST['uri'] : "/";
        $this->request = strtolower($_SERVER['REQUEST_METHOD']);
        $this->routes  = array();
        $this->_request = isset($_REQUEST['_method']) ? strtolower($_REQUEST['_method']) : false;
    }

    /**
     * @param  String $route
     * @param  function $callback Anonymous function
     */
    public function get($route, $callback) {
        $this->routes[__FUNCTION__][] = new Route($route, $callback);
    }
    /**
     * @param  String $route
     * @param  function $callback Anonymous function
     */    
    public function post($route, $callback) {
        $this->routes[__FUNCTION__][] = new Route($route, $callback);
    }  
    /**
     * @param  String $route
     * @param  function $callback Anonymous function
     */    
    public function put($route, $callback) {
        $this->routes[__FUNCTION__][] = new Route($route, $callback);
    }  
    /**
     * @param  String $route
     * @param  function $callback Anonymous function
     */    
    public function delete($route, $callback) {
        $this->routes[__FUNCTION__][] = new Route($route, $callback);
    }  
    /**
     * @param  String $route
     * @param  function $callback Anonymous function
     */        
    public function all($route, $callback) {
        $this->routes[__FUNCTION__][] = new Route($route, $callback);
    }     
    /**
     * @param  function $callback Anonymous function
     */    
    public function error($callback) {
        $this->routes[__FUNCTION__][] = new Route("error", $callback);
    }
 
    public function run() {
        try {   
                if (isset($this->routes[self::$all])) {
                    foreach ($this->routes[self::$all] as $route) {
                        $routeUri = $route->getRoute();
                        preg_match("/^$routeUri$/", $this->uri, $match);    
                        if ($match) {
                            call_user_func($this->routes[self::$all][0]->getCallback());        
                        }
                    }
                }

                foreach ($this->routes[$this->_request ? $this->_request : $this->request] as $route) {
                    $routeUri = $route->getRoute();
                    preg_match("/^$routeUri$/", $this->uri, $match);
                    if ($match) {
                        $match = array_slice($match, 1);  
                        return call_user_func_array($route->getCallback(), array_values($match)); 
                    }
                    
                }
                if (isset($this->routes[self::$error][0])) {
                    return call_user_func($this->routes[self::$error][0]->getCallback());    
                }
                throw new \router\error\NotFoundException("404");

            } catch (\router\error\NotFoundException $e) {
                return $e->getMessage();
            }
    }     
}