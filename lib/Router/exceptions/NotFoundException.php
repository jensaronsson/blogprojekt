<?php 
namespace router\error;
/**
* ErrorController for handling 404
*/
class NotFoundException extends \Exception
{

    public function __construct($message, $code = 0) {
        header("HTTP/1.1 404 Not Found");
        parent::__construct($message, $code);
    }

}